package com.up.selfshop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.up.selfshop.entity.CardEntity;
import com.up.selfshop.mapper.CardEntityMapper;
import com.up.selfshop.res.Res;
import com.up.selfshop.res.ResCardList;

@Controller
@RequestMapping("/card")
public class CardController extends BaseController {
	@Autowired
	private CardEntityMapper cardEntityMapper;

	// 测试用
	@RequestMapping("/")
	public String index() {
		return "user-api.html";
	}

	// 绑卡：
	@RequestMapping(value = "/bind", method = RequestMethod.POST)
	@ResponseBody
	public Res login( //
			@RequestParam String token, //
			@RequestParam String accNo, // 卡号： 6216261000000000018
			@RequestParam String customerNm, // 姓名： 全渠道
			@RequestParam String certifTp, // 证件类型： 01
			@RequestParam String certifId, // 证件号码： 341126197709218366
			@RequestParam String phoneNo, // 手机号： 13552535506
			@RequestParam String cvn2, // cvn2： cvn2-仅贷记卡
			@RequestParam String expired, // 有效期： 有效期-仅贷记卡
			@RequestParam String isDefault, // 设为默认=1
			@RequestParam String nonce, //
			@RequestParam String sign) {
		Res res = new Res();
		return res.signed();
	}

	private String xinxin(String str, int left) {
		if (str != null && str.length() > 4 + left) {
			String start = str.substring(0, left);
			String end = str.substring(str.length() - 4);
			return start + "********" + end;
		} else {
			return str;
		}
	}

	// 卡列表：
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public Res list( //
			@RequestParam String token, //
			@RequestParam String nonce, //
			@RequestParam String sign) {
		ResCardList res = new ResCardList();
		List<CardEntity> cards = cardEntityMapper.selectAll();
		for (int i = 0; i < cards.size(); i++) {
			CardEntity cardEntity = cards.get(i);
			if (i == 0) {
				cardEntity.setIsDefault(1);
			} else {
				cardEntity.setIsDefault(0);
			}
			cardEntity.setAccNo(xinxin(cardEntity.getAccNo(), 6));
			cardEntity.setPhoneNo(xinxin(cardEntity.getPhoneNo(), 3));
			cardEntity.setCertifId(xinxin(cardEntity.getCertifId(), 6));
		}
		res.setCards(cards);
		return res.signed();
	}

	// 设置默认卡：
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public Res update( //
			@RequestParam String token, //
			@RequestParam(defaultValue = "0") Integer cardId, // 卡ID
			@RequestParam(defaultValue = "0") Integer isDefault, // 设为默认=1
			@RequestParam String nonce, //
			@RequestParam String sign) {
		if (cardId <= 0) {
			return Res.err("卡ID错误");
		} else {
			CardEntity cardEntity = cardEntityMapper.selectByPrimaryKey(cardId);
			if (cardEntity == null) {
				return Res.err("卡不存在");
			} else {
				if (isDefault == 1) {
					cardEntity.setIsDefault(1);
					cardEntityMapper.updateByPrimaryKey(cardEntity);
				}
			}
		}
		return new Res().signed();
	}

	// 注销卡：
	@RequestMapping(value = "/cancel", method = RequestMethod.POST)
	@ResponseBody
	public Res cancel( //
			@RequestParam String token, //
			@RequestParam String cardId, // 卡ID
			@RequestParam String nonce, //
			@RequestParam String sign) {
		Res res = new Res();
		return res.signed();
	}
}
