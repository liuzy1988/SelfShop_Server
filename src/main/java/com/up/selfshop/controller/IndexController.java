package com.up.selfshop.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unionpay.acp.sdk.AcpService;
import com.unionpay.acp.sdk.LogUtil;
import com.unionpay.acp.sdk.SDKConfig;
import com.up.selfshop.res.Res;
import com.up.selfshop.util.StrUtil;

@Controller
@RequestMapping("")
public class IndexController extends BaseController {
	static String MER_ID = "777290058152112";
	
	@RequestMapping("/")
	public String index() {
		return "index.html";
	}

	// 验证（商户调用）
	@RequestMapping(value = "/verify", method = RequestMethod.POST)
	@ResponseBody
	public Res verify( //
			@RequestParam String merchat_id, //
			@RequestParam String appkey, //
			@RequestParam String nonce, //
			@RequestParam String sign) {
		Res res = new Res();
		if (!MER_ID.equals(merchat_id)) {
			res.setCode(4);
			res.setMsg("商户不存在");
		} else if (!UserController.TRANS_CODE.equals(appkey)) {
			res.setCode(4);
			res.setMsg("身份码错误或已失效");
		}
		return res.signed();
	}

	// 4.1.4验证回调（APP页面）
	@RequestMapping(value = "/verify_callback")
	public String verify_callback( //
			@RequestParam(defaultValue = "mer_id") String mer_id, //
			@RequestParam String box_id, //
			@RequestParam(defaultValue = "mer_key") String mer_key, //
			Model model) {
		model.addAttribute("mer_id", MER_ID);
		model.addAttribute("box_id", box_id);
		return "verify_callback.html";
	}

	// 4.1.7 订单消费通知（商户调用）
	@RequestMapping(value = "/order_callback", method = RequestMethod.POST)
	@ResponseBody
	public Res order_callback( //
			@RequestParam String order_id, //
			@RequestParam String order_amount, //
			@RequestParam String goods_detail, //
			@RequestParam String nonce, //
			@RequestParam String sign) {
		String trans_result = daishou("merId", "txnAmt", "txnTime", "orderId", "accNo");
		Res res = new Res();
		res.setMsg(trans_result);
		return res.signed();
	}

	@RequestMapping("/backRcvResponse")
	@ResponseBody
	public String backRcvResponse() {
		return "后台通知地址，填写接收银联后台通知的地址，必须外网能访问";
	}

	@RequestMapping("/frontRcvResponse")
	@ResponseBody
	public String frontRcvResponse() {
		return "前台通知地址，填写处理银联前台通知的地址，必须外网能访问";
	}

	// 代收
	@RequestMapping("/daishou")
	@ResponseBody
	public String daishou(//
			@RequestParam String merId, //
			@RequestParam String txnAmt, //
			@RequestParam String txnTime, //
			@RequestParam String orderId, //
			@RequestParam String accNo) {
		merId = "777290058152112"; // 商户号： 777290058152112
		txnAmt = "1000"; // 交易金额 单位为分,不能带小数点
		txnTime = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()); // 订单发送时间
		orderId = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()); // 商户订单号
																				// 自行定义，8-32位数字字母
		accNo = "6216261000000000018"; // 卡号

		Map<String, String> contentData = new HashMap<String, String>();

		/*** 银联全渠道系统，产品参数，除了encoding自行选择外其他不需修改 ***/
		contentData.put("version", SDKConfig.getConfig().getVersion()); // 版本号
		contentData.put("encoding", "UTF-8"); // 字符集编码
														// 可以使用UTF-8,GBK两种方式
		contentData.put("signMethod", SDKConfig.getConfig().getSignMethod()); // 签名方法
																				// 目前只支持01-RSA方式证书加密
		contentData.put("txnType", "11"); // 交易类型
		contentData.put("txnSubType", "02"); // 交易子类型
		contentData.put("bizType", "000501"); // 业务类型
		contentData.put("channelType", "07"); // 渠道类型

		/*** 商户接入参数 ***/
		contentData.put("merId", merId); // 商户号码（商户号码777290058152112仅做为测试调通交易使用，该商户号配置了需要对敏感信息加密）测试时请改成自己申请的商户号，【自己注册的测试777开头的商户号不支持代收产品】
		contentData.put("accessType", "0"); // 接入类型，商户接入固定填0，不需修改
		contentData.put("orderId", orderId); // 商户订单号，8-40位数字字母，不能含“-”或“_”，可以自行定制规则
		contentData.put("txnTime", txnTime); // 订单发送时间，格式为YYYYMMDDhhmmss，必须取当前时间，否则会报txnTime无效
		contentData.put("currencyCode", "156"); // 交易币种（境内商户一般是156 人民币）
		contentData.put("txnAmt", txnAmt); // 交易金额，单位分，不要带小数点
		contentData.put("accType", "01"); // 账号类型

		///////// 不对敏感信息加密使用：
		// contentData.put("accNo",accNo); //这里测试的时候使用的是测试卡号，正式环境请使用真实卡号
		////////

		////////// 如果商户号开通了 商户对敏感信息加密的权限那么，需要对
		////////// 卡号accNo，pin和phoneNo，cvn2，expired加密（如果这些上送的话），对敏感信息加密使用：
		contentData.put("encryptCertId", AcpService.getEncryptCertId());
		String accNoEnc = AcpService.encryptData(accNo, "UTF-8");
		contentData.put("accNo", accNoEnc);
		//////////

		// 后台通知地址（需设置为【外网】能访问 http
		// https均可），支付成功后银联会自动将异步通知报文post到商户上送的该地址，失败的交易银联不会发送后台通知
		// 后台通知参数详见open.unionpay.com帮助中心 下载 产品接口规范 代收产品接口规范 代收交易 商户通知
		// 注意:1.需设置为外网能访问，否则收不到通知 2.http https均可 3.收单后台通知后需要10秒内返回http200或302状态码
		// 4.如果银联通知服务器发送通知后10秒内未收到返回状态码或者应答码非http200，那么银联会间隔一段时间再次发送。总共发送5次，每次的间隔时间为0,1,2,4分钟。
		// 5.后台通知地址如果上送了带有？的参数，例如：http://abc/web?a=b&c=d
		// 在后台通知处理程序验证签名之前需要编写逻辑将这些字段去掉再验签，否则将会验签失败
		contentData.put("backUrl", SDKConfig.getConfig().getBackUrl());

		// 请求方保留域，
		// 透传字段，查询、通知、对账文件中均会原样出现，如有需要请启用并修改自己希望透传的数据。
		// 出现部分特殊字符时可能影响解析，请按下面建议的方式填写：
		// 1. 如果能确定内容不会出现&={}[]"'等符号时，可以直接填写数据，建议的方法如下。
		// contentData.put("reqReserved", "透传信息1|透传信息2|透传信息3");
		// 2. 内容可能出现&={}[]"'符号时：
		// 1) 如果需要对账文件里能显示，可将字符替换成全角＆＝｛｝【】“‘字符（自己写代码，此处不演示）；
		// 2) 如果对账文件没有显示要求，可做一下base64（如下）。
		// 注意控制数据长度，实际传输的数据长度不能超过1024位。
		// 查询、通知等接口解析时使用new String(Base64.decodeBase64(reqReserved),
		// DemoBase.encoding);解base64后再对数据做后续解析。
		// contentData.put("reqReserved",
		// Base64.encodeBase64String("任意格式的信息都可以".toString().getBytes(DemoBase.encoding)));

		/** 对请求参数进行签名并发送http post请求，接收同步应答报文 **/
		Map<String, String> reqData = AcpService.sign(contentData, "UTF-8"); // 报文中certId,signature的值是在signData方法中获取并自动赋值的，只要证书配置正确即可。
		String requestBackUrl = SDKConfig.getConfig().getBackRequestUrl(); // 交易请求url从配置文件读取对应属性文件acp_sdk.properties中的
																			// acpsdk.backTransUrl
		Map<String, String> rspData = AcpService.post(reqData, requestBackUrl, "UTF-8"); // 发送请求报文并接受同步应答（默认连接超时时间30秒，读取返回结果超时时间30秒）;这里调用signData之后，调用submitUrl之前不能对submitFromData中的键值对做任何修改，如果修改会导致验签不通过

		/** 对应答码的处理，请根据您的业务逻辑来编写程序,以下应答码处理逻辑仅供参考-------------> **/
		// 应答码规范参考open.unionpay.com帮助中心 下载 产品接口规范 《平台接入接口规范-第5部分-附录
		if (!rspData.isEmpty()) {
			if (AcpService.validate(rspData, "UTF-8")) {
				LogUtil.writeLog("验证签名成功");
				String respCode = rspData.get("respCode");
				if (("00").equals(respCode)) {
					// 交易已受理(不代表交易已成功），等待接收后台通知更新订单状态,也可以主动发起 查询交易确定交易状态。
					// TODO
					// 如果是配置了敏感信息加密，如果需要获取卡号的铭文，可以按以下方法解密卡号
					// String accNo1 = resmap.get("accNo");
					// String accNo2 = AcpService.decryptData(accNo1, "UTF-8");
					// //解密卡号使用的证书是商户签名私钥证书acpsdk.signCert.path
					// LogUtil.writeLog("解密后的卡号："+accNo2);

				} else if (("03").equals(respCode) || ("04").equals(respCode) || ("05").equals(respCode)) {
					// 后续需发起交易状态查询交易确定交易状态
				} else {
					// 其他应答码为失败请排查原因
				}
			} else {
				LogUtil.writeErrorLog("验证签名失败");
				// TODO 检查验证签名失败的原因
			}
		} else {
			// 未返回正确的http状态
			LogUtil.writeErrorLog("未获取到返回报文或返回http状态码非200");
		}
		String reqMessage = StrUtil.genHtmlResult(reqData);
		String rspMessage = StrUtil.genHtmlResult(rspData);
		return "请求报文:<br/>" + reqMessage + "<br/>" + "应答报文:</br>" + rspMessage;
	}
}
