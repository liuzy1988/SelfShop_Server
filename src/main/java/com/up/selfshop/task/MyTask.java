package com.up.selfshop.task;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class MyTask {
	private Logger logger = Logger.getLogger(MyTask.class);

	public void run() {
		logger.info("MyTask run()");
	}
}
