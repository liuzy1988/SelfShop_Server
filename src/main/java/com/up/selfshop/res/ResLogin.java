package com.up.selfshop.res;

public class ResLogin extends Res {
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
