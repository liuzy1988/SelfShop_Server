package com.up.selfshop.res;

public class ResTransCode extends Res {
	private String transCode;

	public String getTransCode() {
		return transCode;
	}

	public void setTransCode(String transCode) {
		this.transCode = transCode;
	}
}
