package com.up.selfshop.res;

import java.util.ArrayList;
import java.util.List;

import com.up.selfshop.entity.CardEntity;

public class ResCardList extends Res {
	private List<CardEntity> cards = new ArrayList<>();
	public List<CardEntity> getCards() {
		return cards;
	}
	public void setCards(List<CardEntity> cards) {
		this.cards = cards;
	}
}
