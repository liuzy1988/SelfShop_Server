package com.up.selfshop.util;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class SignUtil {

	public static final int start = 10;
	public static final int end = 20;

	public static boolean isSign(Map<String, String> params) {
		String signed = params.get("sign");
		if (signed == null) {
			return false;
		}
		return true;
	}

	public static String sign(Map<String, String> params) {
		SortedMap<String, String> sortedMap = new TreeMap<String, String>();
		sortedMap.putAll(params);
		sortedMap.remove("sign");
		StringBuilder sb = new StringBuilder();
		for (String key : sortedMap.keySet()) {
			sb.append(key).append("=").append(sortedMap.get(key)).append("&");
		}
		String str = sb.length() > 0 ? sb.toString().substring(0, sb.toString().length() - 1) : "";
		return StrUtil.MD5(str).substring(start, end);
	}
}
