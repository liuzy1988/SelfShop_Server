package com.up.selfshop.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import java.util.zip.CRC32;

import com.unionpay.acp.sdk.SDKConstants;

/**
 * 字符串处理工具类
 *
 * @author liuzy
 * @since 2016年6月21日
 */
public class StrUtil {

	private static DecimalFormat df = new DecimalFormat("#.00");

	public static boolean isAmount(BigDecimal bigDecimal) {
		return bigDecimal != null && bigDecimal.doubleValue() > 0;
	}

	public static String round(BigDecimal bigDecimal) {
		return df.format(bigDecimal);
	}

	public static BigDecimal cutZero(BigDecimal bigDecimal) {
		if (bigDecimal == null)
			return new BigDecimal("0.00");
		else
			return new BigDecimal(cutZero(bigDecimal.toString()));
	}

	public static String cutZero(String num) {
		if (num.endsWith("0") && num.indexOf(".") < num.length() - 3) {
			return cutZero(num.substring(0, num.length() - 1));
		}
		return num;
	}

	public static String cutOne(String text) {
		return text.substring(0, text.length() - 1);
	}

	public static String trimT(String text) {
		if (text.startsWith("\t")) {
			text = text.substring(1);
		}
		return text;
	}

	public static String trimRN(String text) {
		if (text.endsWith("\r")) {
			text = cutOne(text);
		}
		if (text.endsWith("\n")) {
			text = cutOne(text);
		}
		return text;
	}

	public static boolean isEmpty(String string) {
		return string == null || string.isEmpty();
	}

	public static boolean isEmpty(Integer num) {
		return num == null || num <= 0;
	}

	public static boolean allEmpty(String... string) {
		for (String str : string) {
			if (!isEmpty(str)) {
				return false;
			}
		}
		return true;
	}

	public static boolean hasEmpty(String... string) {
		for (String str : string) {
			if (isEmpty(str)) {
				return true;
			}
		}
		return false;
	}

	public static byte[] fromHex(String hex) {
		byte[] bytes = new byte[hex.length() / 2];
		for (int i = 0; i < bytes.length; i++) {
			bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		}
		return bytes;
	}

	public static String toHex(byte[] bytes) {
		BigInteger bi = new BigInteger(1, bytes);
		String hex = bi.toString(16);
		int paddingLength = (bytes.length * 2) - hex.length();
		if (paddingLength > 0)
			return String.format("%0" + paddingLength + "d", 0) + hex;
		else
			return hex;
	}

	public static boolean isPhoneNumber(String userName) {
		if (isEmpty(userName))
			return false;
		else
			return Pattern.matches("^[1][3,4,5,7,8][0-9]{9}$", userName);
	}

	public static boolean isUserName(String userName) {
		if (isEmpty(userName)) {
			return false;
		}
		if (isPhoneNumber(userName)) {
			return true;
		}
		return Pattern.matches("^([a-zA-Z][a-zA-Z0-9_]{3,15})$", userName);
	}

	public static Integer parseInt(String str) {
		try {
			return Integer.parseInt(str);
		} catch (Exception e) {
			return null;
		}
	}

	public static String formartMap(Map<String, String> params) {
		StringBuilder sb = new StringBuilder();
		if (params != null && !params.isEmpty()) {
			sb.append("{\n");
			for (String key : params.keySet()) {
				sb.append("  ").append(key).append("=").append(params.get(key)).append("\n");
			}
			sb.append("}");
		}
		return sb.toString();
	}

	public static String SHA1(String str) {
		return getDigestValue(str, "SHA-1");
	}

	public static String MD5(String str) {
		return getDigestValue(str, "MD5");
	}

	private static String getDigestValue(String str, String digestType) {
		try {
			MessageDigest md = MessageDigest.getInstance(digestType);
			md.update(str.getBytes());
			return toHex(md.digest());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String CRC32(String src) {
		CRC32 crc32 = new CRC32();
		crc32.update(src.getBytes());
		return Long.toHexString(crc32.getValue());
	}
	
	public static String genHtmlResult(Map<String, String> data) {

		TreeMap<String, String> tree = new TreeMap<String, String>();
		Iterator<Entry<String, String>> it = data.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> en = it.next();
			tree.put(en.getKey(), en.getValue());
		}
		it = tree.entrySet().iterator();
		StringBuffer sf = new StringBuffer();
		while (it.hasNext()) {
			Entry<String, String> en = it.next();
			String key = en.getKey();
			String value = en.getValue();
			if ("respCode".equals(key)) {
				sf.append("<b>" + key + SDKConstants.EQUAL + value + "</br></b>");
			} else
				sf.append(key + SDKConstants.EQUAL + value + "</br>");
		}
		return sf.toString();
	}
}