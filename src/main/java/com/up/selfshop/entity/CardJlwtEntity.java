package com.up.selfshop.entity;

public class CardJlwtEntity {
    private Integer id;

    private String accNo;

    private String merId;

    private Integer isJlwt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getMerId() {
        return merId;
    }

    public void setMerId(String merId) {
        this.merId = merId;
    }

    public Integer getIsJlwt() {
        return isJlwt;
    }

    public void setIsJlwt(Integer isJlwt) {
        this.isJlwt = isJlwt;
    }
}