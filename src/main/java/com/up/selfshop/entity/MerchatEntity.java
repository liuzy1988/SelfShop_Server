package com.up.selfshop.entity;

import java.util.Date;

public class MerchatEntity {
    private Integer id;

    private String merId;

    private String merUrl;

    private Date cTime;

    private Date utime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMerId() {
        return merId;
    }

    public void setMerId(String merId) {
        this.merId = merId;
    }

    public String getMerUrl() {
        return merUrl;
    }

    public void setMerUrl(String merUrl) {
        this.merUrl = merUrl;
    }

    public Date getcTime() {
        return cTime;
    }

    public void setcTime(Date cTime) {
        this.cTime = cTime;
    }

    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }
}