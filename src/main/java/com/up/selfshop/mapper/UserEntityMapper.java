package com.up.selfshop.mapper;

import java.util.List;
import java.util.Map;

import com.up.selfshop.entity.UserEntity;

public interface UserEntityMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserEntity record);

    UserEntity selectByPrimaryKey(Integer id);

    List<UserEntity> selectAll();

    int updateByPrimaryKey(UserEntity record);
    
    UserEntity findByUname(Map<String, Object> map);
}