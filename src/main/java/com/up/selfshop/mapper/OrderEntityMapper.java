package com.up.selfshop.mapper;

import com.up.selfshop.entity.OrderEntity;
import java.util.List;

public interface OrderEntityMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OrderEntity record);

    OrderEntity selectByPrimaryKey(Integer id);

    List<OrderEntity> selectAll();

    int updateByPrimaryKey(OrderEntity record);
}