package com.up.selfshop.mapper;

import com.up.selfshop.entity.CardEntity;
import java.util.List;

public interface CardEntityMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CardEntity record);

    CardEntity selectByPrimaryKey(Integer id);

    List<CardEntity> selectAll();

    int updateByPrimaryKey(CardEntity record);
}