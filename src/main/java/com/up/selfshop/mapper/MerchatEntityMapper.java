package com.up.selfshop.mapper;

import com.up.selfshop.entity.MerchatEntity;
import java.util.List;

public interface MerchatEntityMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(MerchatEntity record);

    MerchatEntity selectByPrimaryKey(Integer id);

    List<MerchatEntity> selectAll();

    int updateByPrimaryKey(MerchatEntity record);
}