package com.up.selfshop.mapper;

import com.up.selfshop.entity.CardJlwtEntity;
import java.util.List;

public interface CardJlwtEntityMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CardJlwtEntity record);

    CardJlwtEntity selectByPrimaryKey(Integer id);

    List<CardJlwtEntity> selectAll();

    int updateByPrimaryKey(CardJlwtEntity record);
}