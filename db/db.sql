/*!40101 SET NAMES utf8 */;

CREATE TABLE `card_jlwt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accNo` varchar(32) NOT NULL COMMENT '卡号',
  `merId` varchar(32) NOT NULL COMMENT '商户号',
  `isJlwt` int(1) NOT NULL DEFAULT '0' COMMENT '建立委托状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '建立委托状态表';

CREATE TABLE `cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '用户ID',
  `isDefault` int(1) NOT NULL DEFAULT '0' COMMENT '是否默认',
  `accNo` varchar(32) NOT NULL COMMENT '卡号',
  `customerNm` varchar(20) NOT NULL COMMENT '姓名',
  `certifTp` varchar(2) NOT NULL COMMENT '证件类型',
  `certifId` varchar(32) NOT NULL COMMENT '证件号码',
  `phoneNo` varchar(20) NOT NULL COMMENT '手机号',
  `cvn2` varchar(20) DEFAULT NULL COMMENT '贷记卡cvn2',
  `expired` varchar(20) DEFAULT NULL COMMENT '贷记卡有效期',
  `cTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `uTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '用户卡表';

insert  into `cards`(`id`,`userId`,`isDefault`,`accNo`,`customerNm`,`certifTp`,`certifId`,`phoneNo`,`cvn2`,`expired`,`cTime`,`uTime`) values
(1,1,1,'6216261000000000018','全渠道','01','341126197709218366','13552535506',NULL,NULL,'2017-12-15 15:04:46','2017-12-15 15:04:46'),
(2,1,0,'6221558812340000','全渠道','01','341126197709218366','13552535506','123','2311','2017-12-15 15:05:32','2017-12-15 15:05:32');


CREATE TABLE `merchats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merId` varchar(32) NOT NULL COMMENT '商户号',
  `merUrl` varchar(128) NOT NULL COMMENT '商户接口地址',
  `cTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `utime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `merId_index` (`merId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '商户表';

CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '用户ID',
  `orderId` varchar(64) NOT NULL COMMENT '订单号',
  `merId` varchar(20) NOT NULL COMMENT '商户号',
  `boxId` varchar(32) NOT NULL COMMENT '机柜号',
  `amonut` int(11) NOT NULL COMMENT '订单金额',
  `payStat` varchar(10) NOT NULL DEFAULT 'CREATE' COMMENT '支付状态',
  `cTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `uTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `orderId_index` (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '订单表';

CREATE TABLE `user_card_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '用户ID',
  `operate` varchar(10) NOT NULL COMMENT '操作',
  `params` varchar(1024) DEFAULT NULL COMMENT '参数',
  `result` varchar(128) DEFAULT NULL COMMENT '结果',
  `cTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `utime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '用户卡日志';

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(20) NOT NULL,
  `passwd` varchar(20) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `token` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uname_index` (`uname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '用户表';

insert into `users`(`id`,`uname`,`passwd`,`phone`,`token`) values
(1,'test1','123',NULL,NULL);

